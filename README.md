# Wikipedia Explorer Server

Clone the repo and install the dependencies.

```
git clone https://gitlab.com/hoomanf2/wikipedia-explorer-server.git
cd wikipedia-explorer-server
npm install
```

Build and run it:

```
npm run build
npm start
```

Or you can run it in development mode:

```
npm run dev
```

**I usually write test with `jest` for resolvers but because this task was really simple I didn't write the test for it.**
