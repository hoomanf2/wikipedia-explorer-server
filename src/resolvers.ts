import { getArticle } from './query/getArticle';

export const resolvers = {
  Query: {
    getArticle
  }
};
