import axios from 'axios';
import { Context } from './types';

export const context = (): Context => {
  return { axios };
};
