import { ApolloError } from 'apollo-server';
import { env } from 'process';

export const error = (err: Error): Error => {
  // Instead of console.error() we can use Sentry in production
  if (env.NODE_ENV === 'development') console.error(err);
  return new ApolloError('Something Went Wrong. Try Again');
};
