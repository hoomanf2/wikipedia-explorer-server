import { UserInputError } from 'apollo-server';
import { Context, GetArticleArgs, ArticleData } from '../types';
import { error } from '../handler/error';
import { env } from 'process';

export const getArticle = async (
  _: null,
  { page }: GetArticleArgs,
  { axios }: Context
): Promise<ArticleData> => {
  try {
    if (!page || page === '')
      return new UserInputError('page is a required field');

    const article = await axios({
      method: 'get',
      url: env.WIKIPEDIA_API as string,
      params: {
        action: 'parse',
        format: 'json',
        formatversion: 2,
        page
      }
    });

    if (article.data.error?.code) {
      return new UserInputError(article.data.error.info);
    } else {
      return article.data.parse;
    }
  } catch (err) {
    return error(err);
  }
};
