import { gql } from 'apollo-server';
import fs from 'fs';

const query = fs.readFileSync('./src/schema/query.graphql');
const type = fs.readFileSync('./src/schema/type.graphql');

export const typeDefs = gql`
  ${type}
  ${query}
`;
