import { AxiosAdapter } from 'axios';

export interface Context {
  axios: AxiosAdapter;
}

export interface GetArticleArgs {
  page: string;
}

// I can define the article interface but in this case, is not useful
export type ArticleData = unknown | Error;
